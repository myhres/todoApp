var mainApp = angular.module("mainApp", ['ngAnimate', 'ngCookies']);

mainApp.controller('MainController', ['$scope', '$cookies', '$window', function($scope, $cookies, $window) {
    // Reading from cookies
    $scope.list = $cookies.getObject('items') || [];

    // Item object
    $scope.item = {
        content: "",
        completed: false
    };

    let now = new Date();

    // Save item to list
    $scope.submit = function() {
        $scope.list.push({
            content: $scope.item.content,
            completed: $scope.item.completed
        });

        $scope.item.content = ""; // resets text-box
    };

    // Remove item from list
    $scope.remove = function(index) {
        $scope.list.splice(index, 1);
    };

    // Mark all or none of the items as completed
    $scope.toggleAll = function() {
        let toggleStatus = $scope.isAllSelected;
        angular.forEach($scope.list, item => { item.completed = toggleStatus });
    };

    // Checks if all items are completed
    $scope.itemToggled = function() {
        $scope.isAllSelected = $scope.list.every(item => item.completed);
    };

    // Helper function for which items to be shown (filter)
    $scope.toShow = "ALL";
    $scope.show = function(toShow) {
        $scope.toShow = toShow;
    };

    // Remove all completed items
    $scope.removeCompleted = function() {
        $scope.list = $scope.list.filter(item => item.completed === false); 
    };

    // Save items to cookies
    $scope.saveCookies = function() {
        $cookies.putObject('items', $scope.list, {
            expires: new Date(now.getFullYear() + 1, now.getMonth(), now.getDate())
        });
    };

    // Save cookies before leaving page
    $window.onbeforeunload = function(event) {
        $scope.saveCookies();
    };
}]);
